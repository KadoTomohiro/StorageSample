import { Injectable } from '@angular/core';
import { Storage } from './storage';

@Injectable({
  providedIn: 'root'
})
export class MemoryStorageService implements Storage {

  private data: string;

  constructor() {
  }

  set(item: string): void {
    this.data = item;
  }

  delete(): void {
    this.data = undefined;
  }

  get(): string {
    return this.data;
  }
}
