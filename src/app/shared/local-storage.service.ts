import { Injectable } from '@angular/core';
import { Storage } from './storage';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService implements Storage {

  private key = 'localData';
  constructor() { }

  set(data: string): void {
    localStorage.setItem(this.key, data);
  }

  get(): string {
    return localStorage.getItem(this.key);
  }

  delete(): void {
    localStorage.removeItem(this.key);
  }
}
