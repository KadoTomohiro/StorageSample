import { Injectable } from '@angular/core';
import { Storage } from './storage';

@Injectable({
  providedIn: 'root'
})
export class SessionStorageService implements Storage{

  private key = 'localData';
  constructor() { }

  set(data: string): void {
    sessionStorage.setItem(this.key, data);
  }

  get(): string {
    return sessionStorage.getItem(this.key);
  }

  delete(): void {
    sessionStorage.removeItem(this.key);
  }
}
