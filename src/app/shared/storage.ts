export interface Storage {
  set: (item: string) => void;
  get: () => string;
  delete: () => void;
}
