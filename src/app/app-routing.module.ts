import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WriteComponent } from './write/write.component';
import { ReadComponent } from './read/read.component';

const routes: Routes = [
  {path: '', redirectTo: 'write', pathMatch: 'full'},
  {path: 'write', component: WriteComponent},
  {path: 'read', component: ReadComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
