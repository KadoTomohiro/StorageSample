import { Component, OnInit } from '@angular/core';
import { SessionStorageService } from '../shared/session-storage.service';
import { LocalStorageService } from '../shared/local-storage.service';
import { MemoryStorageService } from '../shared/memory-storage.service';

@Component({
  selector: 'ss-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.css']
})
export class ReadComponent implements OnInit {

  session: string;
  local: string;
  memory: string;
  constructor(
    private sessionStorage: SessionStorageService,
    private localStorage: LocalStorageService,
    private memoryStorage: MemoryStorageService
  ) { }

  ngOnInit() {
    this.session = this.sessionStorage.get();
    this.local = this.localStorage.get();
    this.memory = this.memoryStorage.get();
  }

}
