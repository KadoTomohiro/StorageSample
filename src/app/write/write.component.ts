import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SessionStorageService } from '../shared/session-storage.service';
import { LocalStorageService } from '../shared/local-storage.service';
import { MemoryStorageService } from '../shared/memory-storage.service';

@Component({
  selector: 'ss-write',
  templateUrl: './write.component.html',
  styleUrls: ['./write.component.css']
})
export class WriteComponent implements OnInit {

  session = new FormControl('');
  local = new FormControl('');
  memory = new FormControl('');

  constructor(
    private sessionStorage: SessionStorageService,
    private localStorage: LocalStorageService,
    private memoryStorage: MemoryStorageService
  ) {
  }

  ngOnInit() {
    this.sessionFormSet();
    this.localFormSet();
    this.memoryFormSet();
  }

  sessionSubmit(): void {
    this.sessionStorage.set(this.session.value);
  }

  sessionDelete(): void {
    this.sessionStorage.delete();
    this.sessionFormSet();
  }

  private sessionFormSet(): void {
    this.session.setValue(this.sessionStorage.get());
  }

  localSubmit(): void {
    this.localStorage.set(this.local.value);
  }

  localDelete(): void {
    this.localStorage.delete();
    this.localFormSet();
  }

  private localFormSet(): void {
    this.local.setValue(this.localStorage.get());
  }

  memorySubmit(): void {
    this.memoryStorage.set(this.memory.value);
  }

  memoryDelete(): void {
    this.memoryStorage.delete();
    this.memoryFormSet();
  }

  private memoryFormSet(): void {
    this.memory.setValue(this.memoryStorage.get());
  }
}
