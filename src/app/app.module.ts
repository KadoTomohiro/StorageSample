import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WriteComponent } from './write/write.component';
import { ReadComponent } from './read/read.component';
import { NavigatorComponent } from './core/navigator/navigator.component';
import { BaseLayoutComponent } from './core/base-layout/base-layout.component';
import { HeaderComponent } from './core/header/header.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    WriteComponent,
    ReadComponent,
    NavigatorComponent,
    BaseLayoutComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
